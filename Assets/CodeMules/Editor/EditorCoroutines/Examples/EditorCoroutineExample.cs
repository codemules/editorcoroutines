﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using UnityEngine;
using UnityEditor ;

using System.Text;
using System.Collections;

using System;
using System.IO ;

using CodeMules.EditorCoroutines ;

namespace CodeMules.EditorCoroutines.Examples {
	public class EditorCoroutineExample : EditorWindow {
		private StringBuilder text = new StringBuilder() ;
		private Vector2 scroll ;
		private EditorCoroutineManager ecm ;

		[MenuItem("ECR/Test Coroutines")]
		public static void TestEditorCoroutines() {
			EditorCoroutineExample window = EditorWindow.GetWindow<EditorCoroutineExample>();

			#if UNITY_5
			window.titleContent = new GUIContent("Test Editor Coroutines","This is an example of using Editor Coroutines") ;
			#else
			window.title = "Test Editor Coroutines" ;
			#endif

			EditorCoroutineManager.CoroutineAdded += (arg1, arg2) => Debug.Log("Added Coroutine") ;

			window.ecm = new EditorCoroutineManager("Example") ;

			window.ecm.StartCoroutine(WriteSomeText(window.WriteSomeText));
			window.ecm.StartCoroutine(DownloadSomething());
			window.ecm.StartCoroutine(StopAllCoroutines(window.ecm));
		}

		public static IEnumerator WriteSomeText(Action<string> textWriter) {
			while(true) {
				Debug.Log("Running in an editor coroutine!");

				textWriter("Running in an editor coroutine!") ;

				yield return new WaitForSeconds(3.0f);
			}
		}

		public static IEnumerator DownloadSomething() {
			WWW www = new WWW("www.google.com") ;

			yield return www ;

			if (!String.IsNullOrEmpty(www.error)) {
				Debug.Log("Error Downloading: " + www.error) ;
			} else {
				Debug.Log("Download from www.google.com completed successfully: " + www.bytes.Length + " bytes");

				string filePath = Path.GetTempFileName() ;
				filePath = Path.ChangeExtension(filePath,"html") ;
				File.WriteAllBytes(filePath,www.bytes);
				yield return null ;

				Application.OpenURL("file:///" + filePath);
			}
		}

		public static IEnumerator StopAllCoroutines(EditorCoroutineManager ecm) {
			yield return new WaitForSeconds(12.5f);

			Debug.Log("Stopping all Coroutines on " + ecm.Name);

			ecm.StopCoroutines((ecr) => true) ;
		}

		private void WriteSomeText(string someText) {
			text.AppendLine(someText);
			Repaint(); 
		}

		public void OnGUI() {
			scroll = EditorGUILayout.BeginScrollView(scroll);		
			EditorGUILayout.TextArea(text.ToString(), GUILayout.Height(position.height - 30));
			EditorGUILayout.EndScrollView();
		}
	}
}
