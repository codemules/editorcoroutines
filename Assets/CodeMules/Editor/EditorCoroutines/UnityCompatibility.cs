﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

#if UNITY_EDITOR
#if !UNITY_5 || (UNITY_5_0 || UNITY_5_1 || UNITY_5_2)

using System;
using System.Collections ;

namespace CodeMules.EditorCoroutines {
	/// <summary>
	/// Emulation of <see cref="UnityEngine.CustomYieldInstruction"/> available
	/// as of version 5.3
	/// </summary>
	/// <remarks>
	/// This class is only required to maintain source code compatibility for older
	/// versions of Unity (&lt; 5.3).
	/// </remarks>
	public abstract class CustomYieldInstruction : IEnumerator {
		public abstract bool keepWaiting { get ; }

		#region IEnumerator implementation
		public bool MoveNext () {
			return keepWaiting ;
		}

		public void Reset() {
			// NOP
		}

		public object Current {
			get {
				return null ;
			}
		}
		#endregion
	}

	/// <summary>
	/// Emulation of <see cref="UnityEngine.WaitUntil"/>, a <see cref="UnityEngine.CustomYieldInstruction"/>
	/// introducted in version 5.3
	/// </summary>
	/// <remarks>
	/// This class is only required to maintain source code compatibility for older
	/// versions of Unity (&lt; 5.3).
	/// </remarks>
	public class WaitUntil : CustomYieldInstruction {
		public WaitUntil(Func<bool> predicate) {
			if (predicate == null) {
				throw new ArgumentNullException("predicate");
			}

			Predicate = predicate;
		}

		private Func<bool> Predicate { get ; set ; }

		public override bool keepWaiting {
			get {
				return !Predicate();
			}
		}
	}

	/// <summary>
	/// Emulation of <see cref="UnityEngine.WaitWhile"/>, a <see cref="UnityEngine.CustomYieldInstruction"/>
	/// introducted in version 5.3
	/// </summary>
	/// <remarks>
	/// This class is only required to maintain source code compatibility for older
	/// versions of Unity (&lt; 5.3).
	/// </remarks>
	public class WaitWhile : CustomYieldInstruction {
		public WaitWhile(Func<bool> predicate) {
			if (predicate == null) {
				throw new ArgumentNullException("predicate");
			}
			
			Predicate = predicate;
		}
		
		private Func<bool> Predicate { get ; set ; }
		
		public override bool keepWaiting {
			get {
				return Predicate();
			}
		}
	}
}

#endif		// !UNITY_5 || (UNITY_5_0 || UNITY_5_1 || UNITY_5_2)
#endif		// UNITY_EDITOR

