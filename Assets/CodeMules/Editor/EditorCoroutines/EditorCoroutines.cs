﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

#if UNITY_EDITOR
using System;
using System.Collections;

namespace CodeMules.EditorCoroutines {
	/// <summary>
	/// Static class that provides a simple use model for editor coroutines
	/// </summary>
	public static class EditorCoroutines {
		/// <summary>
		/// A default <see cref="EditorCoroutineManager"/> for use when the
		/// user doesnt want to create one and manage it
		/// </summary>
		private static EditorCoroutineManager defaultManager ;

		/// <summary>
		/// Get/Create the default <see cref="EditorCoroutineManager"/>
		/// </summary>
		/// <value>The default manager instance</value>
		private static EditorCoroutineManager DefaultManager {
			get {
				if (defaultManager == null) {
					defaultManager = new EditorCoroutineManager("default") ;
				}

				return defaultManager ;
			}
		}

		/// <summary>
		/// Start an <see cref="EditorCoroutine"/>
		/// </summary>
		/// <param name="coroutine">The coroutine enumeration to execute</param>
		/// <param name="name">An optional name</param>
		/// <param name="owner">An optional owner</param>
		/// <param name="tag">An optional tag identifier</param>
		/// <param name="userData">Optional user data associated with the <c>EditorCoroutine</c></param>
		/// <param name="description">An optional description</param>
		public static EditorCoroutine StartCoroutine(
			IEnumerator coroutine,
			string name = null,
			object owner = null,
			string tag = null,
			object userData = null,
			string description = null
		)
		{
			return DefaultManager.StartCoroutine(
				coroutine,
				name,
				owner,
				tag,
				userData,
				description
				) ;
		}

		/// <summary>
		/// Start an <see cref="EditorCoroutine"/>
		/// </summary>
		/// <returns><paramref name="editorCoroutine"/></returns>
		/// <param name="editorCoroutine">The <see cref="EditorCoroutine"/> to start</param>
		public static EditorCoroutine StartCoroutine(EditorCoroutine editorCoroutine) {
			return DefaultManager.StartCoroutine(editorCoroutine);
		}

		/// <summary>
		/// Stop an <see cref="EditorCoroutine"/>
		/// </summary>
		/// <param name="editorCoroutine">The <see cref="EditorCoroutine"/> to stop</param>
		public static void StopCoroutine(EditorCoroutine editorCoroutine) {
			DefaultManager.StopCoroutine(editorCoroutine);
		}

		/// <summary>
		/// Stop one or more <see cref="EditorCoroutine"/> instances 
		/// specified by a <see cref="Predicate{EditorCoroutine}"/>
		/// </summary>
		/// <param name="selector">The coroutine selector</param>
		public static void StopCoroutines(Predicate<EditorCoroutine> selector) {
			DefaultManager.StopCoroutines(selector);
		}

		/// <summary>
		/// Stop one or more <see cref="EditorCoroutine"/> instances 
		/// based on a tag value
		/// </summary>
		/// <param name="tag">The tag value</param>
		/// <remarks>
		/// <paramref name="tag"/> can be <c>null</c>
		/// </remarks>
		public static void StopCoroutines(string tag) {
			DefaultManager.StopCoroutines(tag);
		}

		/// <summary>
		/// Stop one or more <see cref="EditorCoroutine"/> instances 
		/// based on an owner value
		/// </summary>
		/// <param name="owner">The owner value</param>
		/// <remarks>
		/// <paramref name="owner"/> can be <c>null</c>
		/// </remarks>
		public static void StopCoroutines(object owner) {
			DefaultManager.StopCoroutines(owner);
		}

		/// <summary>
		/// Stop all executing coroutines
		/// </summary>
		public static void StopAllCoroutines() {
			DefaultManager.StopAllCoroutines(); 
		}
	}
}
#endif		// UNITY_EDITOR
