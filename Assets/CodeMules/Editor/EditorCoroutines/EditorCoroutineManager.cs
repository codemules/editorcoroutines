//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

#if UNITY_EDITOR
using UnityEngine;
using UnityEditor ;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CodeMules.EditorCoroutines {

	/// <summary>
	/// <c>EditorCoroutineManager</c> that manages and executes a collection of 
	/// <c>EditorCoroutine</c> instances
	/// </summary>
	/// <remarks>
	/// Use of this class is not required unless you want to isolate sets of coroutines to
	/// sub-spaces. This can be useful if you want to use a derived version of this class,
	/// or you just want to group coroutines via a manager so that they can be stopped as
	/// a group (w/o the use of tags or owners)
	/// </remarks>
	public class EditorCoroutineManager {
		/// <summary>
		/// Lookup to map a yielded type to a <see cref="CustomYieldInstruction"/> wrapper
		/// </summary>
		/// <remarks>
		/// The value of the lookup is a function that takes an object (which was yielded) and
		/// returns the appropriate <see cref="CustomYieldInstruction"/>. In some cases the
		/// value may be <c>null</c> indicating that there is no useful mapping, or the 
		/// mapping would be superfulous.
		/// <para>
		/// This is public so that custom mappings can be added. Anything added or removed
		/// affects all instances of <c>EditorCoroutineManager</c> as the lookup is <c>static</c>
		/// </para>
		/// </remarks>
		public static readonly IDictionary<Type,Func<object,CustomYieldInstruction>> YieldWrappers = 
			new Dictionary<Type, Func<object, CustomYieldInstruction>>
		{
			// WaitForSeconds => CustomWaitForSeconds
			{ typeof(WaitForSeconds),(obj) => new InternalWaitForSeconds(obj as WaitForSeconds) },
			// No wrapper
			{ typeof(WaitForFixedUpdate),null },
			// No wrapper
			{ typeof(WaitForEndOfFrame),null },
			// Identity 
			{ typeof(WaitUntil),(obj) => obj as CustomYieldInstruction },
			// Identity 
			{ typeof(WaitWhile),(obj) => obj as CustomYieldInstruction },
			// WWW => WaitUntil
			{ typeof(WWW),(obj) => new WaitUntil(() => (obj as WWW).isDone) },
			// AsyncOperation => WaitUntil
			{ typeof(AsyncOperation),(obj) => new WaitUntil(() => (obj as AsyncOperation).isDone) },
			// EditorCoroutine => WaitUntil
			{ typeof(EditorCoroutine),(obj) => new WaitUntil(() => (obj as EditorCoroutine).IsFinal) }
		} ;

		/// <summary>
		/// event hook for each coroutine added for every instance of <c>EditorCoroutineManager</c>
		/// </summary>
		/// <remarks>
		/// This event is designed specifically for tools that track execution of editor coroutines
		/// </remarks>
		public static event Action<EditorCoroutineManager,EditorCoroutine> CoroutineAdded ;

		/// <summary>
		/// See <see cref="Coroutines"/>
		/// </summary>
		private IList<EditorCoroutine> coroutines ;

		/// <summary>
		/// Initializes a new instance of the <see cref="EditorCoroutines.EditorCoroutineManager"/> class.
		/// </summary>
		/// <param name="name">The name for the <c>EditorCoroutineManager</c></param>
		public EditorCoroutineManager(string name) {
			if (name == null) {
				throw new ArgumentNullException("name") ;
			}

			if (name.Trim().Length == 0) {
				throw new ArgumentException("name cannot be empty","name") ;
			}

			Name = name ;
		}

		/// <summary>
		/// Get the name of the <c>EditorCoroutineManager</c>
		/// </summary>
		/// <value>The name of the <c>EditorCoroutineManager</c></value>
		public string Name { get ; private set ; }

		/// <summary>
		/// Start an <see cref="EditorCoroutine"/>
		/// </summary>
		/// <returns><paramref name="editorCoroutine"/></returns>
		/// <param name="editorCoroutine">The <see cref="EditorCoroutine"/> to start</param>
		public EditorCoroutine StartCoroutine(EditorCoroutine editorCoroutine) {
			if (editorCoroutine == null) {
				throw new ArgumentNullException("editorCoroutine") ;
			}

			// use internal add method
			AddCoroutine(editorCoroutine) ;

			// return the coroutine
			return editorCoroutine ;
		}

		/// <summary>
		/// Start an <see cref="EditorCoroutine"/>
		/// </summary>
		/// <param name="coroutine">The coroutine enumeration to execute</param>
		/// <param name="name">An optional name</param>
		/// <param name="owner">An optional owner</param>
		/// <param name="tag">An optional tag identifier</param>
		/// <param name="userData">Optional user data associated with the <c>EditorCoroutine</c></param>
		/// <param name="description">An optional description</param>
		public EditorCoroutine StartCoroutine(
			IEnumerator coroutine,
			string name = null,
			object owner = null,
			string tag = null,
			object userData = null,
			string description = null
		) {
			EditorCoroutine editorCoroutine = new EditorCoroutine(
				coroutine,
				name,
				owner,
				tag,
				userData,
				description
			);

			return StartCoroutine(editorCoroutine) ;
		}

		/// <summary>
		/// Stop an <see cref="EditorCoroutine"/>
		/// </summary>
		/// <param name="coroutine">The <see cref="EditorCoroutine"/> to stop</param>
		public void StopCoroutine(EditorCoroutine coroutine) {
			if (coroutine == null) {
				throw new ArgumentNullException("coroutine") ;
			}

			// mark the coroutine as cancelled
			coroutine.Cancel();
		}

		/// <summary>
		/// Stop one or more <see cref="EditorCoroutine"/> instances 
		/// specified by a <see cref="Predicate{EditorCoroutine}"/>
		/// </summary>
		/// <param name="selector">The coroutine selector</param>
		public void StopCoroutines(Predicate<EditorCoroutine> selector) {
			if (selector == null) {
				throw new ArgumentNullException("selector") ;
			}

			foreach(var coroutine in coroutines) {
				// selector says cancel?
				if (selector(coroutine)) {
					coroutine.Cancel();
				}
			}
		}

		/// <summary>
		/// Stop one or more <see cref="EditorCoroutine"/> instances 
		/// based on a tag value
		/// </summary>
		/// <param name="tag">The tag value</param>
		/// <remarks>
		/// <paramref name="tag"/> can be <c>null</c>
		/// </remarks>
		public void StopCoroutines(string tag) {
			StopCoroutines((ec) => ec.Tag == tag);
		}

		/// <summary>
		/// Stop one or more <see cref="EditorCoroutine"/> instances 
		/// based on an owner value
		/// </summary>
		/// <param name="owner">The owner value</param>
		/// <remarks>
		/// <paramref name="owner"/> can be <c>null</c>
		/// </remarks>
		public void StopCoroutines(object owner) {
			StopCoroutines((ec) => ec.Owner == owner);
		}

		/// <summary>
		/// Stop all executing coroutines
		/// </summary>
		public void StopAllCoroutines() {
			StopCoroutines((ec) => true);
		}

		/// <summary>
		/// Primary loop for executing editor coroutines
		/// </summary>
		protected virtual void ProcessCoroutines() {
			// select everything that is not final (i.e., active)
			var activeCoroutines = from p in Coroutines where !p.IsFinal select p;

			foreach(var coroutine in activeCoroutines) {
				object current;

				// Call the coroutine iterator
				if (coroutine.Process(out current)) {
					// did it yield a result?
					if (current != null) {
						// Process any custom yield result we recognize, and if 
						// we dont process it, try to translate it into a CustomYieldInstruction
						if (!ProcessCustomYieldValue(coroutine,current)) {
							// Translate current into a Yield instruction as appropriate
							coroutine.Yield = TranslateToYield(current);
						}
					}
				}
			}

			// select everything this is final and has a completion handler
			var finalCoroutines = from p in Coroutines where p.IsFinal && (p.CompletionHandler != null) select p ;

			// call the completion handlers, trapping exceptions
			foreach(var coroutine in finalCoroutines) {
				try {
					coroutine.CompletionHandler(coroutine);
				} catch(Exception ex) {
					Debug.LogError(
						string.Format(
							"{0} threw an exception on completion: {1}",
							coroutine,
							ex
						));
				}
			}

			// Reassign the coroutine list to what is still active
			Coroutines = (from p in Coroutines where !p.IsFinal select p).ToList();

			// if we have no coroutines we dont need to hook Update
			if (Coroutines.Count == 0) {
				EditorApplication.update -= ProcessCoroutines ;
			}
		}

		/// <summary>
		/// Determine if the yield result is something custom that we understand how to process
		/// </summary>
		/// <returns>
		/// <c>true</c>, if <paramref name="yieldResult"/> was recognized and processed, <c>false</c> otherwise.
		/// </returns>
		/// <param name="coroutine">the <see cref="EditorCoroutine"/> that yielded the result</param>
		/// <param name="yieldResult">The yield result to process</param>
		/// <remarks>
		/// Currently, the following types are recognized and processed:
		/// <list type="bullet">
		/// <item><see cref="EditorCoroutineResult"/></item>
		/// <item><see cref="EditorCoroutineStatus"/></item>
		/// </list>
		/// <para>
		/// If the number of custom yield results grow then we need to put them in a dictionary
		/// and route them through a <c>Func&lt;object,bool&gt;</c>
		/// </para>
		/// </remarks>
		/// <seealso cref="EditorCoroutineResult"/>
		/// <seealso cref="EditorCoroutineStatus"/>
		protected virtual bool ProcessCustomYieldValue(EditorCoroutine coroutine,object yieldResult) {
			bool result = true ;

			EditorCoroutineResult ecr = yieldResult as EditorCoroutineResult ;
			EditorCoroutineStatus ecs = yieldResult as EditorCoroutineStatus ;

			// if it is a EditorCoroutineResult, assign the result
			if (ecr != null) {
				coroutine.Result = ecr.Result ;

			// if it is an EditorCoroutineStatus update the status/percent complete
			} else if (ecs != null) {
				if (ecs.Message != null) {
					coroutine.StatusMessage = ecs.Message ;
				}

				if (ecs.PercentComplete != null) {
					coroutine.PercentComplete = ecs.PercentComplete.Value ;
				}
			} else {
				// not a custom yield result that we recognize
				result = false ;
			}

			return result ;
		}

		/// <summary>
		/// Attempt to translate a yield result into a <see cref="CustomYieldInstruction"/>
		/// </summary>
		/// <returns>
		/// The <see cref="CustomYieldInstruction"/> or <c>null</c> if there is no translation
		/// </returns>
		/// <param name="yieldedValue">A value return from a coroutine yield</param>
		protected virtual CustomYieldInstruction TranslateToYield(object yieldedValue) {
			// if the yielded value is already a CustomYieldInstruction we dont need to do anything
			CustomYieldInstruction cyi = yieldedValue as CustomYieldInstruction ;

			if (cyi == null) {
				Func<object,CustomYieldInstruction> wrapperCreator;

				if (YieldWrappers.TryGetValue(yieldedValue.GetType(),out wrapperCreator)) {
					// if a wrapper creator is specified, use it to create the CustomYieldInstruction
					if (wrapperCreator != null) {
						cyi = wrapperCreator(yieldedValue);
					}
				} else {
					Debug.LogWarning(
						string.Format(
							"Editor coroutine yield unsupported type: '{0}'",yieldedValue.GetType()
						));
				}
			}

			return cyi;
		}

		/// <summary>
		/// Internal add wrapper method so that we can detect when we have no coroutines,
		/// and then we do; allows us to hook <c>EditorApplication.update</c> appropriately
		/// </summary>
		/// <param name="coroutine">The <see cref="EditorCoroutine"/></param>
		protected virtual void AddCoroutine(EditorCoroutine coroutine) {
			if(coroutine.State != EditorCoroutineState.Pending) {
				throw new InvalidOperationException("Cannot add editor coroutine that is already active or completed") ;
			}

			if (Coroutines.Contains(coroutine)) {
				throw new InvalidOperationException("Cannot add multiple instances of same editor coroutine") ;
			}

			Coroutines.Add(coroutine) ;

			if (Coroutines.Count == 1) {
				EditorApplication.update += ProcessCoroutines ;
			}

			SendCoroutineAddedEvent(coroutine);
		}

		private void SendCoroutineAddedEvent(EditorCoroutine coroutine) {
			if (CoroutineAdded != null) {
				Delegate [] listeners = CoroutineAdded.GetInvocationList() ;

				foreach(var listener in listeners) {
					listener.DynamicInvoke(this,coroutine);
				}
			}
		}

		/// <summary>
		/// Get/Set the list of <see cref="EditorCoroutine"/> being managed
		/// </summary>
		/// <value>The list of coroutines</value>
		protected IList<EditorCoroutine> Coroutines {
			get {
				if (coroutines == null) {
					coroutines = new List<EditorCoroutine>();
				}

				return coroutines ;
			}

			set {
				coroutines = value ;
			}
		}
	}
}
#endif		// UNITY_EDITOR
