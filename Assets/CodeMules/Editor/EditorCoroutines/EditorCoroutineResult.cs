//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

#if UNITY_EDITOR
using UnityEngine;

namespace CodeMules.EditorCoroutines {
	/// <summary>
	/// A custom yield value that allow the value of <see cref="EditorCoroutine.Result"/>
	/// to be assigned
	/// </summary>
	/// <remarks>
	/// Note that extending <see cref="YieldInstruction"/> is of zero value, but
	/// we do this to increase awareness that this class can be used in a yield
	/// statement for an <see cref="EditorCoroutine"/>
	/// </remarks>
	public class EditorCoroutineResult : YieldInstruction {
		/// <summary>
		/// Initializes a new instance of the <see cref="EditorCoroutines.EditorCoroutineResult"/> class.
		/// </summary>
		/// <param name="result">The result of the <see cref="EditorCoroutine"/></param>
		public EditorCoroutineResult(object result) {
			Result = result ;
		}

		/// <summary>
		/// Get the result
		/// </summary>
		/// <value>The result</value>
		public object Result { get ; private set ; }
	}
}
#endif		// UNITY_EDITOR

