//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

#if UNITY_EDITOR
using UnityEngine;

using System;
using System.Reflection;

namespace CodeMules.EditorCoroutines {
	/// <summary>
	/// Class to wrap the standard <see cref="WaitForSeconds"/> so that we can allow
	/// users to use <see cref="WaitForSeconds"/> transparently. The primary issue being
	/// that <see cref="WaitForSeconds"/> does not expose any functionality or even 
	/// properties (i.e., it is a black box.)
	/// </summary>
	class InternalWaitForSeconds : CustomYieldInstruction {
		public InternalWaitForSeconds(WaitForSeconds waitForSeconds) {
			// reach into the WaitForSeconds instance via reflection and grab the field that
			// holds the value for seconds. This is the only way to get the information
			float seconds = (float) typeof(WaitForSeconds).GetField(
											"m_Seconds",
											BindingFlags.Instance | BindingFlags.NonPublic).
									GetValue(waitForSeconds);

			// Create the absolute target time
			TargetTime = DateTime.UtcNow.AddSeconds(seconds);
		}

		/// <summary>
		/// Gets a value we should keep waiting
		/// </summary>
		/// <value>
		/// <c>true</c> if we have not reached the target time, <c>false</c> otherwise
		/// </value>
		public override bool keepWaiting {
			get {
				return DateTime.UtcNow < TargetTime ;		
			}
		}

		/// <summary>
		/// Get/Set the target time for the wait
		/// </summary>
		/// <value>The target time</value>
		private DateTime TargetTime { get ; set ; }
	}
}
#endif		// UNITY_EDITOR
