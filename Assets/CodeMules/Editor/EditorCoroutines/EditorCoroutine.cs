//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

#if UNITY_EDITOR
using UnityEngine;

using System;
using System.Collections;

namespace CodeMules.EditorCoroutines {
	/// <summary>
	/// Wrapper around a user coroutine that can execute in the editor
	/// </summary>
	public class EditorCoroutine : IEquatable<EditorCoroutine> {
		#region Equality Operators
		public static bool operator ==(EditorCoroutine a,EditorCoroutine b) {
			if (ReferenceEquals(a,null) || ReferenceEquals(b,null)) {
				return ReferenceEquals(a,b) ;
			}

			return a.Id == b.Id ;
		}

		public static bool operator !=(EditorCoroutine a,EditorCoroutine b) {
			return !(a == b);
		}
		#endregion

		#region Constructor(s)
		/// <summary>
		/// Initializes a new instance of the <see cref="EditorCoroutines.EditorCoroutine"/> class.
		/// </summary>
		/// <param name="coroutine">The coroutine enumeration to execute</param>
		/// <param name="name">An optional name</param>
		/// <param name="owner">An optional owner</param>
		/// <param name="tag">An optional tag identifier</param>
		/// <param name="userData">Optional user data associated with the <c>EditorCoroutine</c></param>
		/// <param name="description">An optional description</param>
		public EditorCoroutine(
			IEnumerator coroutine,
			string name = null,
			object owner = null,
			string tag = null,
			object userData = null,
			string description = null
			)
		{
			// coroutine is *required*
			if (coroutine == null) {
				throw new ArgumentNullException("coroutine") ;
			}

			Enumerator = coroutine ;

			// Generate a unique id for identity purposes
			Id = Guid.NewGuid() ;

			Name = name ?? "(unnamed)" ;
			Owner = owner;
			Tag = tag ;
			UserData = userData ;

			// synthesize a description if one isnt provided
			Description = String.IsNullOrEmpty(description) ?
				string.Format("Editor Coroutine '{0}:{1}'",Name,Id) :
				description;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Get the unique identifier assigned to the <c>EditorCoroutine</c>
		/// </summary>
		/// <value>The unique identifier</value>
		/// <remarks>
		/// This value is used for equality comparisons
		/// </remarks>
		public Guid Id { get ; private set ; }

		/// <summary>
		/// Get/Set the name of the <c>EditorCoroutine</c>
		/// </summary>
		/// <value>The name of the <c>EditorCoroutine</c></value>
		public string Name { get ; set ; }

		/// <summary>
		/// Get/Set the description for the <c>EditorCoroutine</c>
		/// </summary>
		/// <value>The description of the <c>EditorCoroutine</c></value>
		public string Description { get ; set ; }

		/// <summary>
		/// Get/Set an owner for the <c>EditorCoroutine</c>
		/// </summary>
		/// <value>The owner of the <c>EditorCoroutine</c>, or <c>null</c></value>
		public object Owner { get ; set ; }

		/// <summary>
		/// Get/Set the tag identifier for the <c>EditorCoroutine</c>
		/// </summary>
		/// <value>The tag for the <c>EditorCoroutine</c>, or <c>null</c></value>
		/// <remarks>
		/// Tag identifiers are a good way to group related co-routines so that they can
		/// be cancelled as a group
		/// </remarks>
		public string Tag { get ; set ; }

		/// <summary>
		/// Get/Set optional user data assocated with the <c>EditorCoroutine</c>s
		/// </summary>
		/// <value>The user data associated with the <c>EditorCoroutine</c>, or <c>null</c></value>
		public object UserData { get ; set ; }

		/// <summary>
		/// Get/Set an optional status message for the <c>EditorCoroutine</c>
		/// </summary>
		/// <value>The most recent status message</value>
		/// <remarks>
		/// This value is intended to be set when an <c>EditorCoroutine</c> yields an
		/// <see cref="EditorCoroutineState"/> result or a <see cref="string"/> value
		/// </remarks>
		/// <seealso cref="EditorCoroutineStatus"/>
		public string StatusMessage { get ; set ; }

		/// <summary>
		/// Gets or sets the percent complete.
		/// </summary>
		/// <value>The percent complete.</value>
		public float PercentComplete { get ; set ; }

		/// <summary>
		/// Get the <see cref="Exception"/> thrown by the <c>EditorCoroutine</c>
		/// </summary>
		/// <value>The exception</value>
		/// <remarks>
		/// When an <c>EditorCoroutine</c> throws an <see cref="Exception"/> it is
		/// considered completed, and has a state of <see cref="EditorCoroutineState.Error"/>
		/// </remarks>
		public Exception Error { get; private set ; }

		/// <summary>
		/// Get the result of an <c>EditorCoroutine</c>
		/// </summary>
		/// <value>The result</value>
		/// <remarks>
		/// An <c>EditorCoroutine</c> can assign a result value by yield an instance of 
		/// <see cref="EditorCoroutineResult"/>
		/// <para>
		/// This value may be <c>null</c> because it was not assigned, or because it
		/// was assigned the value of <c>null</c>
		/// </para>
		/// </remarks>
		/// <seealso cref="EditorCoroutineResult"/>
		public object Result { get ; internal set ; }

		/// <summary>
		/// Get the current state of the <c>EditorCoroutine</c>
		/// </summary>
		/// <value>The current state of the <c>EditorCoroutine</c></value>
		public EditorCoroutineState State { get ; private set ; }

		/// <summary>
		/// Get/Set a completion handler for the <c>EditorCoroutine</c>
		/// </summary>
		/// <value>The completion handler, or <c>null</c> if not specified</value>
		/// <remarks>
		/// The completion handler is called when the <c>EditorCoroutine</c> 
		/// transitions to a final <see cref="EditorCoroutineState"/>
		/// </remarks>
		/// <seealso cref="IsFinal"/>
		public Action<EditorCoroutine> CompletionHandler { get ; set; }
		#endregion

		#region State Helpers
		/// <summary>
		/// Determine if the <c>EditorCoroutine</c> completed executing
		/// </summary>
		/// <value>
		/// <c>true</c> if the <c>EditorCoroutine</c> completed executing,
		/// <c>false</c> otherwise
		/// </value>
		public bool IsFinal {
			get {
				return State >= EditorCoroutineState.Complete ;
			}
		}

		/// <summary>
		/// Determine if the <c>EditorCoroutine</c> was cancelled/stopped
		/// </summary>
		/// <value>
		/// <c>true</c> if the <c>EditorCoroutine</c> was cancelled/stopped,
		/// <c>false</c> otherwise
		/// </value>
		public bool IsCancelled {
			get {
				return State == EditorCoroutineState.Cancelled ;
			}
		}

		/// <summary>
		/// Determine if the <c>EditorCoroutine</c> completed with an error
		/// </summary>
		/// <value>
		/// <c>true</c> if the <c>EditorCoroutine</c> completed with an error,
		/// <c>false</c> otherwise
		/// </value>
		/// <seealso cref="Error"/>
		public bool IsError {
			get {
				return State == EditorCoroutineState.Error ;
			}
		}

		/// <summary>
		/// Determine if the <c>EditorCoroutine</c> completed successfully
		/// </summary>
		/// <value>
		/// <c>true</c> if the <c>EditorCoroutine</c> completed successfully,
		/// <c>false</c> otherwise
		/// </value>
		/// <seealso cref="Result"/>
		public bool IsSuccess {
			get {
				return State == EditorCoroutineState.Complete ;
			}
		}

		/// <summary>
		/// Determine if the <c>EditorCoroutine</c> is currently executing
		/// </summary>
		/// <value>
		/// <c>true</c> if the <c>EditorCoroutine</c> pending or still executing,
		/// <c>false</c> otherwise
		/// </value>
		public bool IsExecuting {
			get {
				return State <= EditorCoroutineState.Complete ;
			}
		}
		#endregion

		#region Methods
		/// <summary>
		/// Cancel the <c>EditorCoroutine</c>
		/// </summary>
		public void Cancel() {
			// if the coroutine hasnt completed, mark it as cancelled
			if (!IsFinal) {
				State = EditorCoroutineState.Cancelled;
			}
		}
		#endregion

		#region Implementation
		/// <summary>
		/// Get/Set the user coroutine <see cref="IEnumerator"/>
		/// </summary>
		/// <value>The user coroutine enumerator</value>
		private IEnumerator Enumerator { get ; set ; }

		/// <summary>
		/// The current <see cref="CustomYieldInstruction"/> that is blocking
		/// the <c>EditorCoroutine</c>
		/// </summary>
		/// <value>
		/// The yield instruction, or <c>null</c> if the <c>EditorCoroutine</c>
		/// is not blocked
		/// </value>
		internal CustomYieldInstruction Yield { get ; set ; }

		/// <summary>
		/// Process the next iteration of the user coroutine
		/// </summary>
		/// <param name="current">Holds the result of the iteration</param>
		/// <remarks>
		/// <para>
		/// If the <c>EditorCoroutine</c> is blocked then <paramref name="current"/>
		/// will always be <c>null</c>. Otherwise, it will be the result yield from
		/// the user coroutine (which may be <c>null</c>)
		/// </para>
		/// </remarks>
		internal bool Process(out object current) {
			// initialize out parameter
			current = null ;

			// If cancel/stop was requested and we still manage to 
			// get here, terminate ourselves
			if (State == EditorCoroutineState.Cancelled)
				return false ;

			// are we blocking on a yield instruction?
			if (Yield != null) {
				// note that we are yielding
				State = EditorCoroutineState.Yielding ;

				// iterate the yield to see if the yield is satisfied
				if (Yield.MoveNext()) {
					// we are still yielding. Note that current is null
					return true ;
				}
			}

			// Note that we are executing
			State = EditorCoroutineState.Active ;

			try {
				// Iterate the user coroutine
				if (Enumerator.MoveNext()) {
					// return the user coroutine current
					current = Enumerator.Current ;
				} else {
					// User coroutine is complete
					State = EditorCoroutineState.Complete ;
				}
			} catch(Exception ex) {
				Debug.LogError(
					string.Format(
						"{0} threw an exception on completion: {1}",
						this,
						ex
					));

				// Note that we completed in error
				State = EditorCoroutineState.Error ;

				// Save the error in case it is useful
				Error = ex ;
			}

			// We want to continue processing if we are still active (not complete, not error)
			return State == EditorCoroutineState.Active ;
		}
		#endregion

		#region IEquatable implementation
		/// <summary>
		/// Determines whether the specified <see cref="EditorCoroutines.EditorCoroutine"/> is equal to the current <see cref="EditorCoroutines.EditorCoroutine"/>.
		/// </summary>
		/// <param name="other">The <see cref="EditorCoroutines.EditorCoroutine"/> to compare with the current <see cref="EditorCoroutines.EditorCoroutine"/>.</param>
		/// <returns><c>true</c> if the specified <see cref="EditorCoroutines.EditorCoroutine"/> is equal to the current
		/// <see cref="EditorCoroutines.EditorCoroutine"/>; otherwise, <c>false</c>.</returns>
		public bool Equals (EditorCoroutine other) {
			return this == other ;
		}
		#endregion

		#region Overrides
		/// <summary>
		/// Determines whether the specified <see cref="System.Object"/> is equal to the current <see cref="EditorCoroutines.EditorCoroutine"/>.
		/// </summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with the current <see cref="EditorCoroutines.EditorCoroutine"/>.</param>
		/// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to the current
		/// <see cref="EditorCoroutines.EditorCoroutine"/>; otherwise, <c>false</c>.</returns>
		public override bool Equals(object obj) {
			return this == (obj as EditorCoroutine) ;
		}

		/// <summary>
		/// Serves as a hash function for a <see cref="EditorCoroutines.EditorCoroutine"/> object.
		/// </summary>
		/// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a hash table.</returns>
		public override int GetHashCode() {
			return Id.GetHashCode();
		}
		#endregion
	}
}
#endif		// UNITY_EDITOR

