//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

#if UNITY_EDITOR
namespace CodeMules.EditorCoroutines {
	/// <summary>
	/// States for editor coroutines
	/// </summary>
	/// <remarks>
	/// The order of the states is of some importance as we want to be able
	/// to do comparisons against state ranges (for e.g., &gt;= Complete)
	/// </remarks>
	public enum EditorCoroutineState {
		/// <summary>
		/// The coroutine is created, but not started
		/// </summary>
		Pending,
		/// <summary>
		/// The coroutine is active (processing)
		/// </summary>
		Active,
		/// <summary>
		/// The coroutine is yielding (blocked)
		/// </summary>
		Yielding,
		/// <summary>
		/// The coroutine completed (success)
		/// </summary>
		Complete,
		/// <summary>
		/// The coroutine completed with an error
		/// </summary>
		Error,
		/// <summary>
		/// The coroutine was cancelled/stopped
		/// </summary>
		Cancelled
	}
}
#endif		// UNITY_EDITOR

